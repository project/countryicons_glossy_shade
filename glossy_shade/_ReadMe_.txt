+++++++++++++++++++++++++++
+                         +
+ Glossy shade flag icons +
+                         +
+++++++++++++++++++++++++++

Icon set for the Drupal module "Country Icons" (http://drupal.org/project/countryicons) 6.x-1.x.
Version 2010-03-05

Compiled by Robert Allerstorfer (http://drupal.org/user/405360)

Installation
============

Copy the "glossy_shade" folder into the module's "iconsets" directory. 

Usage
=====

To display a flag image, you could use a code similar like this, for example in your theme's "node.tpl.php" file:

  $my_countrycode_alpha2_lc = 'at';
  if ($my_countrycode_alpha2_lc == 'uk') {
    $my_countrycode_alpha2_lc = 'gb';
  }
  $my_countrycode_alpha2 = strtoupper($my_countrycode_alpha2_lc);
  if (module_exists('countryicons')) {
    print theme_countryicons_icon($my_countrycode_alpha2_lc, 'glossy_shade', $my_countrycode_alpha2, $my_countrycode_alpha2);
  }

Image information
=================

Created by Nordic Factory (www.nordicfactory.com) in December 2009. Can be used freely. 
However, the creator asks to link to their website (http://www.nordicfactory.com/) 
on the site where these icons are being displayed! 

Source: http://www.nordicfactory.com/creative/illustrations/download-free-flag-set-in-png/
  (flag_set_256x256_glossy_shade.zip)

Renamed files: "nm.png" -> "mm.png"
               "uk.png" -> "gb.png"
               "vh.png" -> "va.png"

Added files:   "bv.png" = copy of "no.png"
               "gf.png" = copy of "fr.png"
               "um.png" = copy of "us.png"

Files for some countries are missing, including az, cc, cs, eh, gp, hm, mk, pe, ps, re, tf, tk.

All images have been compressed using OptiPNG (http://optipng.sourceforge.net/).


